// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyDaPAvXoSYXW6R_Bnq6GvLnvoh9ASxzB_4",
    authDomain: "appbazar-dd9a4.firebaseapp.com",
    databaseURL: "https://appbazar-dd9a4.firebaseio.com",
    projectId: "appbazar-dd9a4",
    storageBucket: "",
    messagingSenderId: "315853691289"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
