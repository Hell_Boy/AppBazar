import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppstoreService } from '../items/appstore.service';

@Component({
  selector: 'app-appstore',
  templateUrl: './appstore.component.html',
  styleUrls: ['./appstore.component.css']
})
export class AppstoreComponent implements OnInit {

  public platformId;
  public appTypes = [
                        {'id':'1' , name: 'Productivity'},
                        {'id':'2' , name: 'Entertainment'},
                        {'id':'3' , name: 'News'},
                        {'id':'4' , name: 'Networking'},
                        {'id':'5' , name: 'Games'},
                        {'id':'6' , name: 'Utilities'},
                        {'id':'7' , name: 'Life Style'},
                      ];

  public storeApps = [];

  constructor(private route : ActivatedRoute, private _appStoreService : AppstoreService) { }

  ngOnInit() {

    let platformId = parseInt(this.route.snapshot.paramMap.get('platformId'));
    this.platformId = platformId;

    /**Service */

    this.storeApps = this._appStoreService.getAndroidApps();
  }

  onSelected_Type(tabId){
    console.log(tabId);
  }

}
