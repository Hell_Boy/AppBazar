import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  platforms = [
    {"id": 1, "title": "Android" },
    {"id": 2, "title": "IOS" },
    {"id": 3, "title": "Hybrid" }
  ]

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSelect_Nav(platform){
    this.router.navigate(['/appstore',platform.id]);
  }

  onSelect_Platform(i){
    // console.log(i);
    this.router.navigate(['/appstore',i]);
  }

}
