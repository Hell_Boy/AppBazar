import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable({
  providedIn: 'root'
})
export class AppstoreService {

  androidApps = [
    {"id": 1, "title" : 'First', "logo" : "" },
    {"id": 2, "title" : 'Second', "logo" : "" },
    {"id": 3, "title" : 'Third', "logo" : "" },
    {"id": 4, "title" : 'Fourth', "logo" : "" },
    {"id": 5, "title" : 'Fifth', "logo" : "" },
    {"id": 6, "title" : 'Sixth', "logo" : "" },
    {"id": 7, "title" : 'Seventh', "logo" : "" },
    {"id": 8, "title" : 'Eighth', "logo" : "" },
    {"id": 9, "title" : 'Ninth', "logo" : "" },
    {"id": 10, "title" : 'tenth', "logo" : "" },
  ]

  constructor(db: AngularFireDatabase) { }

  getAndroidApps(){

    return this.androidApps;
  }
}
